**NOTE** For Ubuntu 16.04, use v1.0


```
#!shell
git ckeckout v1.0
```

----

**NOTE** 
NOW this thing has been wrapped in a ROS package for convenience.
The firmware layer is in the arduino directory.

----

### HARDWARE###

To build a robot you need (see http://www.dis.uniroma1.it/~spqr/MARRtino/ for more details)

* Arduino Mega 2560
* Arduino motor shield (or L298 equivanet)
* two motors with encoders
* a 12V battery
* a frame to fix all these devices
* a laptop to connect to Arduino board

### QUICK CONNECTION GUIDE ###

**Motor A**

* connect the power to the A channel of the motor shield
* connect the encoder A and B pins to the pins 52/53 of the Arduino Mega
* connect the power of the encoder (5V)

**Motor B**

* connect the power to the B channel of the motor shield
* connect the encoder A and B pins to the pins 51/52 of the Arduino Mega
* connect the power of the encoder (5V)


###COMPILING AND UPLOADING FIRMWARE TO ARDUINO###

0. sudo apt-get install arduino arduino-mk
1. cd arduino_robot/arduino
2. edit the Makefile to specify where arduino is connected
     MONITOR_PORT = /dev/ttyACMXXX 
3. make
4. make upload (use sudo if /dev/ttyACM0 is not writeable)


###COMPILING FOR LINUX (no ROS)###
To compile the bare tools for linux (no ROS)

0. sudo apt-get install libncurses5-dev libwebsockets-dev
1. cd arduino_robot/src
2. make
3. sudo make install (Optional: if you want to copy libarduino_robot.so in /usr/local/lib)
4. sudo ldconfig     (Optional: if you want to use libarduino_robot.so from /usr/local/lib)

this will yield four files:

*    libarduino_robot.so: the connection library to be linked by your programs
*    arduino_robot_monitor: an ncurses based terminal to control the platform
*    arduino_robot_shell: a shell to set the parameters and write them in eeprom
*    arduino_robot_websocket_server: a web based server for controlling the robot via browser


###COMPILING FOR ROS ###

0. sudo apt-get install libncurses5-dev
1. add thin_drivers/thin_msgs to your catkin_ws (https://bitbucket.org/ggrisetti/thin_drivers) to your workspace
2. add arduino_robot to your workspace
3. catkin_make

this will yield the three files of the ROSless installation plus an arduino_robot_node


###PLATFORM SETUP###

**Fix the directions of the encoders/pwm (after uploading the program in the arduino)**

Setting udev rule for Arduino board:

Create a file (e.g., /etc/udev/rules.d/90-arduino.rules) with the following line
```
#Arduino MEGA
SUBSYSTEMS=="usb", ATTRS{idProduct}=="0042", ATTRS{idVendor}=="2341", SYMLINK+="arduino arduino_mega_$attr{serial}", MODE="0666"
SUBSYSTEMS=="usb", ATTRS{idProduct}=="0242", ATTRS{idVendor}=="2341", SYMLINK+="arduino arduino_mega_$attr{serial}", MODE="0666"
```

Note: for Arduino clones check the idProduct with 'dmesg' command.


On the PC, run
```
./arduino_robot_monitor /dev/ttyACM<your serial port>
```
you should see a screen with some numbers wabbling.

There are four modes available

0. (hit '0') shows the system status, and the core timer period
1. (hit '1') shows the pwm control of the motors
  up/down left/right allow to control the joints
2. (hit '2') is PID joint mode
  up/down left/right allow to control the desired speed of the motors
3. (differential drive mode)
  up/down left/right allow to control the rotational 
  and translational velocity of the base

To see if you need to flip the encoders issue a positive pwm to the each motor
(mode 1, arrow keys). If there is a sign mismatch between the pwm and the encoder speed reported in the terminal, then you have to flip the encoder

After doing that, you can check the PID mode (key 2)
There are some parameters to be set
* - pid coefficients (multiplied by 100, as i do integer arithmetics on the arduino1)
* - max encoder speed per period (ticks/period)
* - min/max pwm
* - max slope of the speed ramp per period

Finally, you can enjoy with the differential drive mode (key 3).
There are a couple of  ways you can mount the motors.
Tell the system what is the left joint and the right joint for the wheels,
the meters-per-tick of left and right wheel (kl, kr) and the distance between
the wheels (baseline).
Also the motors can be mounted in different directions, and this will result in kl/kr of changing signs.

The parameters can be set with another utility that is the

```
./arduino_robot_shell <port>
```

Here you can twickle with the parameters and save them to the eeprom of arduino.

###ALTERNATIVE CONFIGURATION###

I added a more handy configuration system that requires
* a web browser
* a binary compiled in the src directory

This seems to be easier to use:

1. run the server
```
./arduino_robot_websocket_server /dev/ttyACM0
```

1. open with a browser the html file in the src directory
```   
firefox index_controller.html
```

That should do.

###EXTENDING FIRMWARE###

4. to add custom packets you have to
   1. add your POD (no virtual methods) structures in packets.h
   2. define the appropriate handling routines in the handlePackets.
   3. never remove the #pragma push and pop

###ROBOT PROGRAM###

The file robot_program.ccp contains examples of simple control programs.
9 functions robot_control_program_<i> are defined (at the end of file robot_program.cpp) 
and each function <i> can be executed with the command

```
./robot_program <i>
```

Feel free to implement and test your control program.


Enjoy,
	G.