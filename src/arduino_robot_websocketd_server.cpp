#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <curses.h>
#include "arduino_robot_connection.h"
#include <iostream>
#include <sstream>
#include <map>
#include <pthread.h>
#include "arduino_robot_connection.h"
#include "command_parser.h"

using namespace std;

ArduinoRobotConnection connection;

void printResponsePacket(std::string& buf, const ResponsePacket& p){
  ostringstream  os;
  os << "<b> RESPONSE </b> s: " << p.seq 
     << " id: " << (int) p.src_id 
     << " e: " << (int) p.error_code << "<br/>";
  buf=os.str();
}


#define printAttribute(os,struct_name,name)				\
  os<< "<tr><td valign=\"top\"> "<< #name << "</td><td style=\"min-width:120px\" >" << struct_name.name <<"</td/></tr>"

#define printAttributeNoStruct(os,name)				\
  os<< "<tr><td valign=\"top\"> "<< #name << "</td><td style=\"min-width:120px\" >" << name <<"</td></tr>"

void printSystemStatusPacket(std::string& buf, const SystemStatusPacket& p){
  ostringstream  os;
  os <<"<table border=\"1\">";
  printAttribute(os,p,seq);
  printAttribute(os,p,rx_packets);
  printAttribute(os,p,rx_packet_errors);
  printAttribute(os,p,watchdog_count);
  printAttribute(os,p,battery_level);
  printAttribute(os,connection,packetCount());
  printAttribute(os,connection,packetErrors());
  os <<"</table>";
  buf=os.str();
}

void printSystemParamsPacket(std::string& buf, const SystemParamPacket& p){
  ostringstream  os;
  os <<"<table border=\"1\">";
  printAttribute(os,p,seq);
  printAttribute(os,p,comm_speed);
  printAttribute(os,p,comm_cycles);
  printAttribute(os,p,watchdog_cycles);
  int motor_mode=p.motor_mode;
  printAttributeNoStruct(os,motor_mode);
  printAttribute(os,p,timer_period);
  os <<"</table>";
  buf=os.str();
}

void printJointStatusPacket(std::string& buf, const JointStatusPacket& p){
  ostringstream  os;
  os <<"<table border=\"1\">";
  printAttribute(os,p,seq);
  os <<"<tr><th> num </th><th>attributes</th></tr>"; 
  for(int i=0; i<num_motors; i++){
    os <<"<tr> <td> " << i << "</td><td><table border=\"1\">";
    const JointInfo& joint=p.joints[i];
    printAttribute(os,joint,mode);
    printAttribute(os,joint,encoder_position);
    printAttribute(os,joint,encoder_speed);
    printAttribute(os,joint,desired_speed);
    printAttribute(os,joint,pwm);
    printAttribute(os,joint,sensed_current);
    os << "</table></td></tr>";
  }
  os <<"</table>";
  buf=os.str();
}


void printJointParamsPacket(std::string& buf, const JointParamPacket& p){
  ostringstream  os;
  os <<"<table border=\"1\">";
  printAttribute(os,p,seq);
  os <<"<tr><th> num </th><th>parameters</th></tr>"; 
  for(int i=0; i<num_motors; i++){
    os <<"<tr> <td> " << i << "</td><td><table border=\"1\">";
    const JointParams& joint=p.params[i];
    printAttribute(os,joint,kp);
    printAttribute(os,joint,ki);
    printAttribute(os,joint,kd);
    printAttribute(os,joint,max_speed);
    printAttribute(os,joint,min_pwm);
    printAttribute(os,joint,max_pwm);
    printAttribute(os,joint,slope);
    os << "</table></td></tr>";
  }
  os <<"</table>";
  buf=os.str();
}

void printKinematicsParamsPacket(std::string& buf, const DifferentialDriveParamPacket& p){
  ostringstream  os;
  os <<"<table border=\"1\">";
  printAttribute(os,p,seq);
  printAttribute(os,p,baseline);
  printAttribute(os,p,kr);
  printAttribute(os,p,kl);
  int right_joint_index=p.right_joint_index;
  int left_joint_index=p.left_joint_index;
  int use_on_board_odometry=p.use_on_board_odometry;
  printAttributeNoStruct(os,right_joint_index);
  printAttributeNoStruct(os,left_joint_index);
  printAttributeNoStruct(os,use_on_board_odometry);
  os <<"</table>";
  buf=os.str();
}

void printKinematicsStatus(std::string& buf, const DifferentialDriveStatusPacket& p){
  ostringstream  os;
  os <<"<table border=\"1\">";
  printAttribute(os,p,seq);
  printAttribute(os,p,odom_x);
  printAttribute(os,p,odom_y);
  printAttribute(os,p,odom_theta);
  printAttribute(os,p,translational_velocity);
  printAttribute(os,p,rotational_velocity);
  os <<"</table>";
  buf=os.str();
}

bool run;
unsigned const int BUF_SIZE=1024;
std::string response_string;
std::string system_status_string;
std::string joint_status_string;
std::string kinematic_status_string;
std::string system_params_string;
std::string joint_params_string;
std::string kinematic_params_string;

void* connectionThread(void*){
  while(run){
    connection.spinOnce();
    printResponsePacket(response_string,connection.lastResponse());
    printSystemStatusPacket(system_status_string,connection.systemStatus());
    printJointStatusPacket(joint_status_string,connection.jointStatus());
    printKinematicsStatus(kinematic_status_string,connection.kinematicsStatus());
    printSystemParamsPacket(system_params_string,connection.systemParams());
    printJointParamsPacket(joint_params_string,connection.jointParams());
    printKinematicsParamsPacket(kinematic_params_string,connection.kinematicsParams());
    cout << "<p> <table border=\"3\">";
    cout << "<tr>";
    cout << "<th> System Status  </th>";
    cout << "<th> System Params  </th>";
    cout << "<th> Joint Status   </th>";
    cout << "<th> Joint Params   </th>";
    cout << "<th> Kinematic Status  </th>";
    cout << "<th> Kinematic Params  </th>";
    cout << "</tr>";
    cout << "<tr>";
    cout << "<td valign=\"top\">" << system_status_string << "</td>";
    cout << "<td valign=\"top\">" << system_params_string << "</td>";
    cout << "<td valign=\"top\">" << joint_status_string << "</td>";
    cout << "<td valign=\"top\">" << joint_params_string << "</td>";
    cout << "<td valign=\"top\">" << kinematic_status_string << "</td>";
    cout << "<td valign=\"top\">" << kinematic_params_string << "</td>";
    cout << "</tr>";
    cout << "</table> </p>";
    cout << "<p>" << response_string <<  "</p>" << endl;
  }
}

/*
class BaseCommandHandler{
public:
  BaseCommandHandler(const std::string& tag, ArduinoRobotConnection* connection) {
    _connection=connection;
    _tag=tag;
  }
  const std::string tag() const {return _tag;}
  virtual bool handleCommand(istream& is)=0;
  virtual std::string help() const {return _tag;}
  virtual ~BaseCommandHandler(){};
protected:
  ArduinoRobotConnection* _connection;
  std::string _tag;
};

typedef std::map<std::string, BaseCommandHandler*> CommandMap;
CommandMap command_map;

class HelpCommandHandler: public BaseCommandHandler{
public:
  HelpCommandHandler():BaseCommandHandler("help", 0){
  }
  virtual bool handleCommand(istream& is) { 
    for (CommandMap::iterator it=command_map.begin(); it!=command_map.end(); it++){
      cout << it->second->help() << endl;
    }
    return true;
  }
protected:
  bool* _run;
};

class QuitCommandHandler: public BaseCommandHandler{
public:
  QuitCommandHandler(ArduinoRobotConnection* c, bool* run_variable):
    BaseCommandHandler("quit", c){
    _run=run_variable;
  }
  virtual bool handleCommand(istream& is) { *_run = false; return true;}
protected:
  bool* _run;
};


class PrintParamCommandHandler: public BaseCommandHandler{
public:
  PrintParamCommandHandler(ArduinoRobotConnection* c):
    BaseCommandHandler("printParams", c){}
  virtual bool handleCommand(istream& is) { 
    int value;
    is >> value;
    if (! is)
      return false;
    switch(value){
    case 0: 
      //printSystemParamPacket(_connection->systemParams());
      break;
    case 1: 
      //printJointParamPacket(_connection->jointParams());
      break;
    case 2: 
      //printKinematicsParamPacket(_connection->kinematicsParams());
      break;
    default:
      return false;
    }
  }
protected:
  bool* _run;
};


#define CALL_COMMAND_1_CLASS(command)\
  class Call##command: public BaseCommandHandler{\
  public:\
  Call##command(ArduinoRobotConnection* connection):	\
  BaseCommandHandler(#command, connection){} \
  virtual std::string help() const {return _tag + " <command_arg>";} \
  virtual bool handleCommand(istream& is) { \
    int value;							\
    is >> value;						\
    if (! is)						\
      return false;						\
    cout << "argument: " << value << endl;			\
    return _connection->command(value);				\
  }								\
  };								\

#define GETTER_COMMAND_CLASS(command)\
  class Get##command: public BaseCommandHandler{\
  public:\
  Get##command(ArduinoRobotConnection* connection):	\
  BaseCommandHandler(#command, connection){} \
  virtual bool handleCommand(istream& is) { \
    cout << #command << "=" << (float) _connection->command() << endl;	\
    return true;\
  }\
  };\

#define GETTER_COMMAND_1_CLASS(command)\
  class Get##command: public BaseCommandHandler{\
  public:\
  Get##command(ArduinoRobotConnection* connection):	\
  BaseCommandHandler(#command, connection){} \
  virtual std::string help() const {return _tag + " <joint_num>";} \
  virtual bool handleCommand(istream& is) { \
    int value;\
    is >> value; \
    if (! is) \
      return false; \
    cout << #command << "=" << _connection->command(value) << endl; \
    return true; \
  }\
  };\

#define SETTER_COMMAND_CLASS(command)\
  class Set##command: public BaseCommandHandler{\
  public:\
  Set##command(ArduinoRobotConnection* connection):	\
  BaseCommandHandler(#command, connection){} \
  virtual std::string help() const {return _tag + " <value>";} \
  virtual bool handleCommand(istream& is) { \
    float value;\
    is >> value; \
    if (! is) \
      return false; \
    return _connection->command (value);\
  }\
  };\

#define SETTER_COMMAND_2_CLASS(command)\
  class Set##command: public BaseCommandHandler{\
  public:\
  Set##command(ArduinoRobotConnection* connection):	\
  BaseCommandHandler(#command, connection){} \
  virtual std::string help() const {return _tag + " <joint_num> <value>";} \
  virtual bool handleCommand(istream& is) { \
    float v1, v2;\
    is >> v1; \
    if (! is) \
      return false; \
    is >> v2; \
    if (! is) \
      return false; \
    return _connection->command (v1, v2);\
  }\
  };\

class SetJointsPWM: public BaseCommandHandler{
public:
  SetJointsPWM(ArduinoRobotConnection* connection):
    BaseCommandHandler("setJointsPWM",connection){}
  virtual std::string help() const {return _tag + " <pwm0> <pwm1> ...";} 
  virtual bool handleCommand(istream& is) { 
    int16_t pwm[num_motors];
    for (int i=0; i<num_motors; i++)
      is >> pwm[i];
    connection.setJointPWM(pwm);
  }
};

class SetJointsSpeeds: public BaseCommandHandler{
public:
  SetJointsSpeeds(ArduinoRobotConnection* connection):
    BaseCommandHandler("setJointsSpeed",connection){}
  virtual std::string help() const {return _tag + " <speed0> <speed1> ...";} 
  virtual bool handleCommand(istream& is) { 
    int16_t speeds[num_motors];
    for (int i=0; i<num_motors; i++)
      is >> speeds[i];
    connection.setJointSpeeds(speeds);
  }
};

class SetBaseVelocities: public BaseCommandHandler{
public:
  SetBaseVelocities(ArduinoRobotConnection* connection):
    BaseCommandHandler("setBaseVelocities",connection){}
  virtual std::string help() const {return _tag + " <tv> <rv>";} 
  virtual bool handleCommand(istream& is) { 
    float tv, rv;
    is >> tv;
    is >> rv;
    connection.setBaseVelocities(tv,rv);
  }
};




template <typename T>
void addCommand(T command){
  command_map.insert(std::make_pair(command->tag(), command));
}

bool callCommand(const std::string& tag, istream& is){
  CommandMap::iterator it=command_map.find(tag);
  if (it==command_map.end())
    return false;
  return it->second->handleCommand(is);
}

GETTER_COMMAND_CLASS(timerPeriod);
GETTER_COMMAND_CLASS(commCycles);
GETTER_COMMAND_CLASS(watchdogCycles);
SETTER_COMMAND_CLASS(setCommCycles);
SETTER_COMMAND_CLASS(setWatchdogCycles);
GETTER_COMMAND_CLASS(systemSeq);
GETTER_COMMAND_CLASS(serverRxPackets);
GETTER_COMMAND_CLASS(serverTxPackets);
GETTER_COMMAND_CLASS(serverRxErrors);
GETTER_COMMAND_CLASS(numJoints);
GETTER_COMMAND_CLASS(jointSeq);
GETTER_COMMAND_CLASS(kinematicsSeq);
GETTER_COMMAND_CLASS(kr);
GETTER_COMMAND_CLASS(kl);
GETTER_COMMAND_CLASS(baseline);
GETTER_COMMAND_CLASS(rightMotorIndex);
GETTER_COMMAND_CLASS(leftMotorIndex);
GETTER_COMMAND_CLASS(x);
GETTER_COMMAND_CLASS(y);
GETTER_COMMAND_CLASS(theta);
GETTER_COMMAND_CLASS(translationalVelocity);
GETTER_COMMAND_CLASS(rotationalVelocity);
GETTER_COMMAND_CLASS(motorMode);


GETTER_COMMAND_1_CLASS(minPWM);
GETTER_COMMAND_1_CLASS(maxPWM);
GETTER_COMMAND_1_CLASS(kp);
GETTER_COMMAND_1_CLASS(ki);
GETTER_COMMAND_1_CLASS(kd);
GETTER_COMMAND_1_CLASS(maxSpeed)
GETTER_COMMAND_1_CLASS(maxI);
GETTER_COMMAND_1_CLASS(slope);

SETTER_COMMAND_2_CLASS(setMinPWM);
SETTER_COMMAND_2_CLASS(setMaxPWM);
SETTER_COMMAND_2_CLASS(setKp);
SETTER_COMMAND_2_CLASS(setKi);
SETTER_COMMAND_2_CLASS(setKd);
SETTER_COMMAND_2_CLASS(setMaxSpeed)
SETTER_COMMAND_2_CLASS(setMaxI);
SETTER_COMMAND_2_CLASS(setSlope);
SETTER_COMMAND_CLASS(setBaseline);
SETTER_COMMAND_CLASS(setMotorMode);
SETTER_COMMAND_CLASS(setKr);
SETTER_COMMAND_CLASS(setKl);
SETTER_COMMAND_CLASS(setRightMotorIndex);
SETTER_COMMAND_CLASS(setLeftMotorIndex);

CALL_COMMAND_1_CLASS(pushParams);
CALL_COMMAND_1_CLASS(loadParams);
CALL_COMMAND_1_CLASS(queryParams);
CALL_COMMAND_1_CLASS(saveParams);



void fillCommandMap(){
  addCommand(new QuitCommandHandler(&connection, &run));
  addCommand(new HelpCommandHandler());
  addCommand(new GettimerPeriod(&connection));

  addCommand(new GetcommCycles(&connection));
  addCommand(new SetsetCommCycles(&connection));

  addCommand(new GetwatchdogCycles(&connection));
  addCommand(new SetsetWatchdogCycles(&connection));

  addCommand(new SetsetMotorMode(&connection));
  addCommand(new GetmotorMode(&connection));

  addCommand(new GetsystemSeq(&connection));
  addCommand(new GetserverRxPackets(&connection));
  addCommand(new GetserverTxPackets(&connection));
  addCommand(new GetserverRxErrors(&connection));

  addCommand(new GetnumJoints(&connection));

  addCommand(new GetminPWM(&connection));
  addCommand(new GetmaxPWM(&connection));
  addCommand(new Getkp(&connection));
  addCommand(new Getki(&connection));
  addCommand(new Getkd(&connection));
  addCommand(new GetmaxSpeed(&connection));
  addCommand(new Getslope(&connection));
  addCommand(new GetmaxI(&connection));

  addCommand(new SetsetMinPWM(&connection));
  addCommand(new SetsetMaxPWM(&connection));
  addCommand(new SetsetKp(&connection));
  addCommand(new SetsetKi(&connection));
  addCommand(new SetsetKd(&connection));
  addCommand(new SetsetMaxSpeed(&connection));
  addCommand(new SetsetSlope(&connection));
  addCommand(new SetsetMaxI(&connection));

  addCommand(new GetjointSeq(&connection));
  addCommand(new GetkinematicsSeq(&connection));
  addCommand(new Getkr(&connection));
  addCommand(new Getkl(&connection));
  addCommand(new Getbaseline(&connection));
  addCommand(new GetrightMotorIndex(&connection));
  addCommand(new GetleftMotorIndex(&connection));
  addCommand(new Getx(&connection));
  addCommand(new Gety(&connection));
  addCommand(new Gettheta(&connection));
  addCommand(new GettranslationalVelocity(&connection));
  addCommand(new GetrotationalVelocity(&connection));
  addCommand(new SetsetKr(&connection));
  addCommand(new SetsetKl(&connection));
  addCommand(new SetsetBaseline(&connection));
  addCommand(new SetsetRightMotorIndex(&connection));
  addCommand(new SetsetLeftMotorIndex(&connection));
  
  addCommand(new CallpushParams(&connection));
  addCommand(new CallqueryParams(&connection));
  addCommand(new CallloadParams(&connection));
  addCommand(new CallsaveParams(&connection));
  addCommand(new PrintParamCommandHandler(&connection));

  addCommand(new SetJointsPWM(&connection));
  addCommand(new SetJointsSpeeds(&connection));
  addCommand(new SetBaseVelocities(&connection));
}

*/

int main(int argc, char** argv) {
  fillCommandMap(&connection);
  connection.connect(argv[1]);
  if (! connection.isConnected()){
    return 0;
  }
  bool result;
  cout << "querying system params ... ";
  result=connection.queryParams(0);
  cout << result << endl;

  cout << "querying joint params ... ";
  result=connection.queryParams(1);
  cout << result << endl;

  cout << "querying kinematic params ... ";
  result=connection.queryParams(2);
  cout << result << endl;

  
  cerr <<  endl;
  cout << "READY" << endl;
  
  run = true;
  pthread_t runner;
  pthread_create(&runner, 0, &connectionThread, 0);
  while (run) {
    cout << "abot> ";
    char buf[1024];
    cin.getline(buf, 1024);
    istringstream is(buf);
    std::string tag;
    is >> tag;
    bool result = callCommand(tag, is);
    if (! result){
      cout << "error" << endl;
    } else 
      cout << "ok" << endl;
  }
  void* retval;
  pthread_join(runner, &retval);
  return 0;
}
