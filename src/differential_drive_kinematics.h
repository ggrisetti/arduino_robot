#pragma once
#include <stdint.h>
class DifferentialDriveKinematics{
public:
  DifferentialDriveKinematics(float kr_=1e-3, float kl_=1e-3, float baseline_=0.3);
  void setParams(float kr_, float kl_, float baseline_);
  inline float kl() const {return _kl;}
  inline float kr() const {return _kr;}
  inline float baseline() const {return _baseline;}
  inline float x() const {return _x;}
  inline float y() const {return _y;}
  inline float theta() const {return _theta;}
  inline float translationalVelocity() const {return _translational_velocity;}
  inline float rotationalVelocity() const { return _rotational_velocity;}
  
  void update(uint16_t right_encoder, uint16_t left_encoder, float time_interval);
  void reset(float x_=0, float y_=0, float theta_=0);
  void velocity2ticks(int16_t& right_ticks, int16_t& left_ticks, 
		      float translational_velocity, float rotatonal_velocity,
		      float time_interval);
protected:
  float _previous_left_encoder, _previous_right_encoder;
  float _x, _y, _theta;
  float _kl, _kr, _baseline;
  float _ikl, _ikr, _ibaseline;
  float _translational_velocity, _rotational_velocity;
};

